<?php

namespace fruktozets\dropzone\behaviors;

use fruktozets\dropzone\File;
use fruktozets\dropzone\IAttachable;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;
use yii;

/**
 * Class MultipleFilesBehavior
 *
 * @property \yii\db\ActiveQuery $fileQB
 * @property array $fileIds
 * @property array $whereCondition
 * @property ActiveRecord $owner
 *
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package common\widgets
 */
class MultipleFilesBehavior extends BaseFileBehavior
{
    /**
     * The model linking the image and the owner.
     *
     * @var IAttachable|ActiveRecord
     */
    public $linkModelClass;

    /**
     * Properties for getting file ids from table linking the image and the owner.
     * Also, these properties be used for insert into the database.
     *
     * ```php
     *  [
     *      'type' => 'avatar',
     *      'status' => 'active',
     * ]
     * ```
     * @var array
     */
    public $additionalProperties;

    /**
     * @var string
     */
    private $linkTableName;

    /**
     * @return array
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_INSERT => 'fileFilter',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'fileFilter',
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (!$this->linkModelClass) {
            throw new \RuntimeException('Properties "linkModelClass" must be set');
        }

        if (!(in_array(IAttachable::class, class_implements($this->linkModelClass)))) {
            throw new \RuntimeException('Property "linkModelClass" must be implements of IAttachable');
        }

        $this->linkTableName = $this->linkModelClass::tableName() . '.';
    }

    /**
     * @return void
     */
    public function fileFilter()
    {

        $fileIds = $this->getFileIds();

        $newFileIds = Json::decode($this->owner[$this->attribute]);

        if ($forDelete = array_filter(array_diff($fileIds, $newFileIds))) {
            $this->linkModelClass::deleteAll([
                $this->linkModelClass::getFileField() => $forDelete,
            ]);

            foreach ($this->fileClass::findAll($forDelete) as $file) {
                $file->delete();
            };
        }

        if ($forInsert = array_filter(array_diff($newFileIds, $fileIds))) {
            foreach ($forInsert as $fileId) {
                (new $this->linkModelClass(array_merge($this->additionalProperties, [
                    $this->linkModelClass::getFileField() => $fileId,
                    $this->linkModelClass::getOwnerField() => $this->owner->getPrimaryKey(),
                ])))->save();
            };
        }

        $this->fileClass::updateAll(['status' => $this->fileClass::STATUS_ACTIVE], ['id' => $newFileIds]);
    }

    /**
     * @param string $basePath
     * @param string $baseUrl
     * @return array
     */
    public function getMockFiles($basePath = "@webroot", $baseUrl = "@web")
    {
        $fileData = $this->getFileQB()
            ->select(new Expression('f.id, 
                CONCAT(f.base_name, \'.\', f.extension) as name, 
                CONCAT(f.path,\'' . File::FILE_NAME_ORIGINAL . '\', \'.\', f.extension) as path'))
            ->leftJoin([
                'f' => $this->fileClass::tableName(),
            ], $this->linkTableName . $this->linkModelClass::getFileField() . " = f.id")
            ->asArray()
            ->all();

        $files = [];

        foreach ($fileData as $fileDatum) {
            $path = Yii::getAlias($basePath . $fileDatum['path']);

            $files[] = [
                'id' => $fileDatum['id'],
                'name' => $fileDatum['name'],
                'size' => filesize($path),
                'thumbnail' => Yii::getAlias($baseUrl . $fileDatum['path']),
            ];
        }

        return $files;
    }

    /**
     * @return array
     */
    public function getFileIds()
    {
        return $this->getFileQB()
            ->select($this->linkModelClass::getFileField())
            ->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileQB()
    {
        return $this->linkModelClass::find()
            ->where([
                $this->linkTableName . $this->linkModelClass::getOwnerField() => $this->owner->primaryKey,
            ])
            ->andFilterWhere($this->getWhereCondition());
    }

    /**
     * @return array
     */
    private function getWhereCondition()
    {
        $condition = [];

        foreach ($this->additionalProperties as $property => $value) {
            $condition[$this->linkTableName . $property] = $value;
        }

        return $condition;
    }
}