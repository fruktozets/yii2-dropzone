<?php

namespace fruktozets\dropzone\behaviors;

use fruktozets\dropzone\File;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;
use yii;

/**
 * Class FileBehavior
 *
 * @property ActiveRecord $owner
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package common\widgets
 */
class FileBehavior extends BaseFileBehavior
{
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'fileValidator',
            BaseActiveRecord::EVENT_AFTER_INSERT => 'updateFile',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'updateFile',
        ];
    }

    public function fileValidator()
    {
        if (is_null($this->owner[$this->attribute])) {
            return;
        }

        $value = Json::decode($this->owner[$this->attribute]);

        if (!is_array($value) || !isset($value[0])) {
            $this->owner[$this->attribute] = null;

            return;
        }

        $this->owner[$this->attribute] = $value[0];
    }

    public function updateFile()
    {
        $this->fileClass::updateAll(['status' => $this->fileClass::STATUS_ACTIVE], ['id' => $this->owner[$this->attribute]]);
    }

    /**
     * @param string $basePath
     * @param string $baseUrl
     * @return array|null
     */
    public function getMockFiles($basePath = "@webroot", $baseUrl = "@web"): array
    {
        $fileData = File::find()
            ->alias('f')
            ->select(new Expression('f.id, 
                CONCAT(f.base_name, \'.\', f.extension) as name, 
                CONCAT(f.path,\'' . File::FILE_NAME_ORIGINAL . '\', \'.\', f.extension) as path'))
            ->where([
                'id' => $this->owner[$this->attribute],
            ])
            ->asArray()
            ->one();

        if (!$fileData) {
            return [];
        }

        $path = Yii::getAlias($basePath . $fileData['path']);

        return [[
            'id' => $fileData['id'],
            'name' => $fileData['name'],
            'size' => filesize($path),
            'thumbnail' => Yii::getAlias($baseUrl . $fileData['path']),
        ]];
    }
}