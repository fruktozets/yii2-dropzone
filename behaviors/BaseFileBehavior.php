<?php

namespace fruktozets\dropzone\behaviors;


use fruktozets\dropzone\File;
use yii\base\Behavior;

/**
 * Class BaseFileBehavior
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package fruktozets\dropzone
 */
class BaseFileBehavior extends Behavior
{
    /**
     * Attribute which contain image ids.
     *
     * @var
     */
    public $attribute;

    /**
     * @var string|File
     */
    public $fileClass = File::class;

    public function init()
    {
        if (!$this->attribute) {
            throw new \RuntimeException('Properties "attribute" must be set');
        }

        if (!$this->fileClass) {
            throw new \RuntimeException('Properties "fileClass" must be set');
        }
    }
}