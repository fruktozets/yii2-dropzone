<?php

namespace fruktozets\dropzone\actions;

use fruktozets\dropzone\File;
use Yii;
use yii\base\Action;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class UploadAction
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package fruktozets\dropzone
 */
class UploadAction extends Action
{
    public $fileName = 'file';
    public $afterUploadHandler = null;
    public $afterUploadData = null;
    public $basePath = '@webroot';
    public $uploadPath = '/uploads/';
    public $baseUrl = '@web';

    /**
     * ['file', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024*1024]
     *
     * @var array
     */
    public $fileRule;

    /**
     * @var string
     */
    public $scenario = File::SCENARIO_IMAGE;

    /**
     * @var bool
     */
    public $saveModel = true;

    public function init()
    {
        parent::init();

        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    /**
     * @return array|string
     * @throws HttpException
     * @throws \yii\base\Exception
     */
    public function run()
    {
        $model = new File([
            'scenario' => $this->scenario,
            'uploadPath' => $this->uploadPath,
        ]);

        if ($this->fileRule) {
            $model->fileRule = $this->fileRule;
        }

        $model->file = UploadedFile::getInstanceByName($this->fileName);

        if (!$model->file || $model->file->hasError) {
            throw new HttpException(500, 'Upload error');
        }

        if (!$model->upload($this->saveModel)) {
            return Json::encode([
                'errors' => $model->getFirstErrors(),
            ]);
        }

        return $this->callUserFunc($model);
    }

    /**
     * @param File $model
     * @return array
     */
    private function callUserFunc(File $model)
    {
        $response = [
            'name' => $model->name,
            'id' => $model->id,
        ];

        if (isset($this->afterUploadHandler)) {
            $data = [
                'data' => $this->afterUploadData,
                'file' => $model->file,
                'dirName' => $this->uploadPath,
                'src' => Yii::getAlias($this->baseUrl . $this->uploadPath),
                'filename' => $model->name,
                'params' => Yii::$app->request->post(),
            ];

            if ($result = call_user_func($this->afterUploadHandler, $data)) {
                $response['afterUpload'] = $result;
            }
        }

        return $response;
    }
}