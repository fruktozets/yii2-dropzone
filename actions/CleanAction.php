<?php

namespace fruktozets\dropzone\actions;

use fruktozets\dropzone\File;
use yii\base\Action;

/**
 * Class CleanAction
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package fruktozets\dropzone
 */
class CleanAction extends Action
{
    /**
     * @return int
     */
    public function run()
    {
        return File::deleteAll([
            'AND',
            ['status' => File::STATUS_UPLOADED],
            ['<', 'created_at', time() - 12 * 3600 ]
        ]);
    }
}