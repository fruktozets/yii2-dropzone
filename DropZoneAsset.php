<?php

namespace fruktozets\dropzone;

use yii\web\AssetBundle;

/**
 * Asset bundle for DropZone Widget
 * 
 * Class DropZoneAsset
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package fruktozets\dropzone
 */
class DropZoneAsset extends AssetBundle
{
    public $sourcePath = '@bower/dropzone/dist/';
    public $js = [
        YII_ENV_DEV ? "dropzone.js" : "min/dropzone.min.js"
    ];
    public $css = [
        YII_ENV_DEV ? "dropzone.css" : "min/dropzone.min.css"
    ];
}