<?php

namespace fruktozets\dropzone;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\InputWidget;
use yii;

/**
 *
 * Class DropZone
 * @see https://www.dropzonejs.com/
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package common\widgets
 */
class DropZone extends InputWidget
{
    const DROPZONE_NAME = "dropzone";

    /**
     *  Client options for DropZone.
     *  See setDefaultClientOptions doc.
     *  [
     *      'addRemoveLinks' => 'true',
     *      'acceptedFiles' => 'image/*',
     *  ]
     * @see https://www.dropzonejs.com/#configuration-options
     * @var array the options for the dropZone JS plugin.
     */
    public $clientOptions = [];

    /**
     * Disable auto discover for all elements
     * @var bool
     */
    public $autoDiscover = false;

    /**
     * @var string
     */
    private $dropzoneName;

    /**
     * @var string
     */
    public $baseUrl = '@web';

    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @see https://www.dropzonejs.com/#events
     * @var array
     */
    public $eventHandlers = [];

    /**
     * @var string
     */
    public $uploadUrl = ['file/upload'];

    /**
     * @var int
     */
    public $maxFiles = 30;

    /**
     * For example:
     *
     *  ```php
     *  [[
     *      'id' => 123,
     *      'name' => foo.jpeg,
     *      'size' => 2048,
     *      'thumbnail' => http://example.com/img/foo120x120.jpg,
     *  ]]
     *  ```
     * If the value is "null", then an attempt will be made to take
     * files from the behavior of the model named as the value of "attribute".
     *
     * @var array|null
     */
    public $files;

    /**
     * @throws yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (\Yii::$app->getRequest()->enableCsrfValidation) {
            $this->options['headers'][\yii\web\Request::CSRF_HEADER] = \Yii::$app->getRequest()->getCsrfToken();
            $this->options['params'][\Yii::$app->getRequest()->csrfParam] = \Yii::$app->getRequest()->getCsrfToken();
        }

        $this->dropzoneName = self::DROPZONE_NAME . '_' . $this->id;
        Html::addCssClass($this->options, 'dropzone');

        $this->setDefaultClientOptions();
        $this->setDefaultEventHandlers();
    }

    private function setDefaultEventHandlers()
    {
        $this->eventHandlers = array_merge([
            'success' => "function(file, response) {
            var fileIds = []; 
            file.id = response.id;
            
            for (var i=0; i<{$this->dropzoneName}.files.length; i++) {
                fileIds.push({$this->dropzoneName}.files[i].id);
            }
           
            document.getElementById('{$this->options['id']}-input').value = JSON.stringify(fileIds);
        }",
            'removedfile' => "function(file) {
             var fileIds = []; 
            
            for (var i=0; i<{$this->dropzoneName}.files.length; i++) {
                fileIds.push({$this->dropzoneName}.files[i].id);
            }
           
            document.getElementById('{$this->options['id']}-input').value = JSON.stringify(fileIds);
        }",
        ], $this->eventHandlers);
    }

    /**
     * Merge options with default values for DropZone js
     */
    private function setDefaultClientOptions()
    {
        $this->clientOptions = array_merge([
            'addRemoveLinks' => 'true',
            'acceptedFiles' => 'image/*',
            'dictDefaultMessage' => Yii::t('yii', 'Drop file here or click to upload'),
            'url' => Url::to($this->uploadUrl),
            'maxFiles' => $this->maxFiles,
//            'thumbnail' => "function(file, dataUrl) {
//                return '<img src=\"' + dataUrl + '\">'
//    },",
        ], $this->clientOptions);
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $inputOptions = [
            'id' => $this->options['id'] . '-input',
        ];

        if ($this->hasModel()) {
            $input = Html::activeHiddenInput($this->model, $this->attribute, $inputOptions);
        } else {
            $input = Html::hiddenInput($this->name, $this->value, $inputOptions);
        }

        $content = '';
        $content .= $input;

        $content .= Html::tag('div', '', $this->options);

        $this->registerClientScript();

        return $content;
    }

    /**
     * Registers dropZone js plugin
     */
    protected function registerClientScript()
    {
        $js = [];
        $view = $this->getView();
        $id = $this->options['id'];
        $autoDiscover = $this->autoDiscover ? 'true' : 'false';
        $clientOptions = Json::encode($this->clientOptions);

        DropZoneAsset::register($view);
        $js[] = "Dropzone.autoDiscover = {$autoDiscover};";
        $js[] = $this->dropzoneName . ' = new Dropzone("#' . $id . '", ' . $clientOptions . ');';

        $files = $this->getExisitingFiles();

        $js[] = $this->addFiles(Json::encode($files));
        $js[] = $this->decrementMaxFiles(count($files));

        foreach ($this->eventHandlers as $event => $handler) {
            $handler = new \yii\web\JsExpression($handler);
            $js[] = $this->dropzoneName . ".on('{$event}', {$handler})";
        }

        $view->registerJs(implode("\n", $js));
    }

    /**
     * @return array
     */
    private function getExisitingFiles()
    {
        if (!is_null($this->files)) {
            return $this->files;
        }

        if ( $this->hasModel() ) {
            $attribute = Html::getAttributeName($this->getName());
            if ($this->model->hasMethod('getMockFiles') && $this->model->getBehavior($attribute)) {
                $files = $this->model->getBehavior($attribute)->getMockFiles();
            }
        }

        return $files ?? [];
    }

    protected function getName()
    {
        return $this->hasModel() ? $this->attribute : $this->name;
    }

    /**
     * @param string $files Json
     *
     * @return string
     */
    protected function addFiles(string $files): string
    {
        $js = [];

        if (empty($files) === false) {
            $js[] = 'var dropzoneInst = ' . $this->dropzoneName;
            $js[] = 'var files = ' . $files;
            $js[] = 'for (var i=0; i<files.length; i++) {';
            $js[] = '   var mockFile = files[i];';
            $js[] = '   dropzoneInst.emit("addedfile", mockFile);';
            $js[] = '   dropzoneInst.files.push(mockFile);';
            //$js[] = '   dropzoneInst.createThumbnailFromUrl(mockFile, mockFile.thumbnail);';
            $js[] = '   dropzoneInst.emit("thumbnail", mockFile, mockFile.thumbnail);';

            $js[] = '   dropzoneInst.emit("complete", mockFile);';
            $js[] = '}';
            $js[] = "var fileIds = []; 
            
            for (var i=0; i<{$this->dropzoneName}.files.length; i++) {
                fileIds.push({$this->dropzoneName}.files[i].id);
            }
           
            document.getElementById('{$this->options['id']}-input').value = JSON.stringify(fileIds);";
        }

        return implode("\n", $js);
    }

    /**
     * @param int $num
     * @return string
     */
    protected function decrementMaxFiles(int $num): string
    {
        if ($num > 0) {
            return "if ({$this->dropzoneName}.options.maxFiles > 0) { 
                {$this->dropzoneName}.options.maxFiles = {$this->dropzoneName}.options.maxFiles - {$num};
                }";
        }

        return '';
    }
}
