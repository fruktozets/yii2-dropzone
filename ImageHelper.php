<?php

namespace fruktozets\dropzone;

use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use yii\helpers\Html;
use yii\imagine\Image;

/**
 * Created by PhpStorm.
 * User: Павел
 * Date: 19.05.2017
 * Time: 21:17
 */
class ImageHelper
{
    private static $baseUrl = '@web';

    /**
     * @param File $image
     * @param int|null $width
     * @param int|null $height
     * @param array $options
     * @return string
     */
    public static function img(File $image, int $width = null, int $height = null, $options = [])
    {
        return Html::img(self::src($image, $width, $height), $options);
    }

    /**
     * @param File $image
     * @param int $width
     * @param int $height
     *
     * @return string
     *
     */
    public static function src(File $image, int $width = null, int $height = null)
    {
        return self::getFile($image, $width, $height) ?: self::getNoImage($width, $height);
    }

    /**
     * @param File $image
     * @param int|null $width
     * @param int|null $height
     * @return bool|string
     */
    public static function getFile(File $image, int $width = null, int $height = null)
    {
        $baseUrl = \Yii::getAlias(self::$baseUrl);
        $original = $image->name;

        if (!file_exists($image->fullPath . $original)) {
            return null;
        }

        if (!$width || !$height) {
            return $baseUrl . $image->path . $original;
        }

        $thumbnail = sprintf( '%dx%d' . '.' . $image->extension, $width, $height);

        if (!file_exists($image->fullPath . $thumbnail)) {
            $photo = Image::getImagine()->open($image->fullPath  . $original);
            $photo
                ->thumbnail(new Box($width, $height), ManipulatorInterface::THUMBNAIL_OUTBOUND)
                ->save($image->fullPath  . $thumbnail, ['quality' => 70]);
        }

        return $baseUrl . $image->path . $thumbnail;
    }

    /**
     * @param int $width
     * @param int $height
     *
     * @return string
     */
    public static function getNoImage($width = null, $height = null)
    {
        return '/resources/images/noimage.png';
        $root = \Yii::getAlias('@webroot');
        $noimage = '/uploads/noimage.gif';

        if (!$width || !$height) {
            return \Yii::getAlias(self::$baseUrl) . $noimage;
        }

        $thumbnail = sprintf('/uploads/img/noimage_%dx%d.gif', $width, $height);

        if (!file_exists($root . $thumbnail)) {
            $photo = Image::getImagine()->open($root . $noimage);
            $photo->thumbnail(new Box($width, $height))->save($root . $thumbnail, ['quality' => 70]);
        }

        return \Yii::getAlias(self::$baseUrl) . $thumbnail;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }
}
