# yii2-dropzone

### Require source
    composer require "fruktozets/yii2-dropzone"

### Execute migration
```php
'controllerMap' => [
    'migrate' => [
        ...
        'migrationNamespaces' => [
            ...
            'fruktozets\dropzone',
        ],
    ],
],
```  
    
### Create a table linking the image and the owner.
```php
$this->createTable('user_to_file', [
    'user_id' => $this->integer(),
    'file_id' => $this->integer(),
    //'type' => $this->smallInteger()->notNull()->defaultValue(2),

]);
```    
### Use widget
```php
echo $form->field($model, 'images')->widget(DropZone::className(), [
    'uploadUrl' => ['site/upload'],
]) 
```

### Use action
```php
public function actions()
{
   return [
       'upload' => [
           'class' => 'fruktozets\dropzone\actions\UploadAction',
           'fileRule' => ['file', 'file', 'extensions' => ['jpg'], 'maxSize' => 1024*1024],
       ],

   ];
}
```

### Implement IAttachable interface
```php
class UserToFile extends \yii\db\ActiveRecord implements IAttachable
```

### Use behavior 
#### WARNING! "behavior" name must be equal "attribute" name! If you want to use MultipleFilesBehavior don't forget to add 'attribute' which associated with files to your model.
Use MultipleFilesBehavior for multiple files linked via third party table
```php
public function behaviors()
{
    $behaviors = parent::behaviors();
    
    $behaviors['images'] = [
        'class' => MultipleFilesBehavior::class,
        'attribute' => 'images',
        'linkModelClass' => UserToFile::class,
        'additionalProperties' => ['type' => UserToFile::TYPE_IMAGE_MAIN,],
    ];

    return $behaviors;
}
```
Use FileBehavior for single file 
```php
public function behaviors()
{
    $behaviors = parent::behaviors();
    
    $behaviors['image_id'] = [
        'class' => FileBehavior::class,
        'attribute' => 'image_id',
    ];

    return $behaviors;
}
```

### Do not forget to add your property to the rules in the model.
```php
public function rules()
{
    return [
        ```
        [['images'], 'safe'],
    ];
}
```