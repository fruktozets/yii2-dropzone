<?php

namespace fruktozets\dropzone;

/**
 * Interface IAttachable
 * @author Pavel Kuznetsov <kuznetsov-web@bk.ru>
 * @package fruktozets\dropzone
 */
interface IAttachable
{
    /**
     * @return string
     */
    public static function getFileField();

    /**
     * @return string
     */
    public static function getOwnerField();
}